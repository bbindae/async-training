using System;
using System.Threading.Tasks;

// This task works just fine in a console app, but will deadlock when called from a GUI
// or Lagacy ASP.NET Context.
// However, this won't dealock in ASP.NET Core since ASP.NET Core does not have AspNetSynchronziationContext and it's a contextless.
// But still you should not block on Asyn Code

public static class DeadlockDemo
{
    private static async Task DelayAsync()
    {
        await Task.Delay(1000);
    }

    public static void Test()
    {
        // Start the delay.
        var delay = DelayAsync();
        // Wait for the delay to complete.
        delay.Wait();
    }
}