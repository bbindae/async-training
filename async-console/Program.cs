﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;

namespace async_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main starts!");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            // to avoid AggregateException
            MainAsync(args).GetAwaiter().GetResult();
            stopWatch.Stop();
            var timeSpan = stopWatch.ElapsedMilliseconds;
            Console.Write($"timeElapsed: {timeSpan}");
            Console.WriteLine("Main ends!");

        }

        static async Task MainAsync(string[] args)
        {
            Console.WriteLine("\t MainAsync starts");
            var taskA = MethodA();
            //await taskA;
            Console.WriteLine("About to call MethodB");
            var taskB = MethodB();
            //await taskB;

            await Task.WhenAll(taskA, taskB);

            Console.WriteLine("\t MainAysnc ends");
        }

        static async Task MethodA()
        {
            Console.WriteLine("\t\t MethodA");
            Console.WriteLine("\t\t MethodA is about to wait");
            await Task.Delay(5000);
            Console.WriteLine("\t\t MethodA is completed");
        }

        static async Task MethodB()
        {
            Console.WriteLine("\t\t MethodB");
            Console.WriteLine("\t\t Method B is about to wait");
            await Task.Delay(2000);
            Console.WriteLine("\t\t MethodB is completed");
        }
    }
}
