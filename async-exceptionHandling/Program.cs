﻿using System;
using System.Threading.Tasks;

namespace async_exceptionHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            try
            {
                MainAsync(args).GetAwaiter().GetResult();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Exception is caught in Main");
                Console.WriteLine($"ErrorType: {ex.GetType()}");
                Console.WriteLine(ex);
            }
            Console.WriteLine("Main ends");      
        }

        static async Task MainAsync(string[] args)
        {

            Console.WriteLine("MainAsync Start!!");
            //try
            //{
                await TestAsync();
            //}
            //catch (System.Exception ex)
            //{
                // Excpetion won't be caught here. Instead the exception will be directly 
                // raised to the current SynchronizationContext.
            //    Console.WriteLine(ex.Message);                
            //}
            Console.WriteLine("MainAsync Ends!!");
        }

        static async Task TestAsync()
        {
            throw new Exception("This is an error message!!!!");
        }
    }
}
